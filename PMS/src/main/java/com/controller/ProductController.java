package com.controller;


import com.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.repo.ProductRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:8080")
@RestController
@RequestMapping("/api")
public class ProductController {

    @Autowired
    ProductRepository repo;

    @GetMapping("products")
    public List<Product> getAllProducts(){

        List<Product> products = new ArrayList<>();

        repo.findAll().forEach(products::add);
        return  products;
    }

//    @GetMapping("products/{id}")
//    public Product getProductById(@PathVariable long id){
//
//
//
//
//        repo.findById(id)
//        return  products;
//    }
    @PostMapping("products/create")
    public Product postProduct(@RequestBody Product product) {

        Product _product = repo.save(
                new Product(product.getId(),
                            product.getProductName(),
                        product.getProductType(),
                        product.getProductCode(),
                        product.isNew(),
                        product.getPrice()));
        return _product;
// constructor  parameters  from com.model
//        this.id = id;
//        this.productName = productName;
//        this.productType = productType;
//        this.productCode = productCode;
//        this.isNew = isNew;
//        this.price = price;

    }

    @DeleteMapping("products/{id}")
    public ResponseEntity<String> deleteProduct(@PathVariable("id") long id){
        repo.deleteById(id);
        return new ResponseEntity<>("Product has been deleted", HttpStatus.OK);
    }

    //LETS delete all products
    @DeleteMapping("products/delete")
    public ResponseEntity<String> deleteAllProducts(){
        System.out.println("deleting all products");
        repo.deleteAll();
        return new ResponseEntity<>("All products have been deleted", HttpStatus.OK);
    }

    @GetMapping(value = "products/productType/{productCode}")
    public List<Product> findByProductType(@PathVariable String productCode){
        List<Product> products = repo.findProductsByProductCode(productCode);
        return products;
    }

    @PutMapping("products/{id}")
    public ResponseEntity<Product> updateProduct(@PathVariable("id") long id,
        @RequestBody Product product){

        System.out.println("Update product with id: " + id + "..." );

        Optional<Product> productData = repo.findById(id);

        if(productData.isPresent()){
            Product _prod = productData.get();
            _prod.setPrice(product.getPrice());
            _prod.setProductCode(product.getProductCode());
            _prod.setProductName(product.getProductName());
            _prod.setNew(product.isNew());
            return new ResponseEntity<>(repo.save(_prod), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
