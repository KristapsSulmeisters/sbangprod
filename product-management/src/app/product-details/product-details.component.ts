import { Component, OnInit, Input } from '@angular/core';
import { ProductService } from '../product.service';
import { Product } from '../product';

import { ProductsListComponent } from '../products-list/products-list.component';

@Component({
  selector: 'product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {

  @Input() product: Product;

  constructor(private productService: ProductService, private listComponent: ProductsListComponent) { }

  ngOnInit() {
  }

  updateActive(isNew: boolean) {
    this.productService.updateProduct(this.product.id,
      {
        productName: this.product.productName,
        productCode: this.product.productCode,
        price: this.product.price,
        productType: this.product.productType,
        isNew: isNew

      })
      .subscribe(
        data => {
          console.log(data);
          this.product = data as Product;
        },
        error => console.log(error));
  }

  deleteProduct() {
    this.productService.deleteProduct(this.product.id)
      .subscribe(
        data => {
          console.log(data);
          this.listComponent.reloadData();
        },
        error => console.log(error));
  }

}
