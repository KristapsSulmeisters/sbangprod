export class Product{
  id:number;
  isNew: boolean;
  price: number;
  productCode: string;
  productName: string;
  productType: string;
}
