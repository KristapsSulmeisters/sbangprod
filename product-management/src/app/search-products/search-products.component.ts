import { Component, OnInit } from '@angular/core';
import { Product } from '../product';
import { ProductService } from '../product.service';

@Component({
  selector: 'search-products',
  templateUrl: './search-products.component.html',
  styleUrls: ['./search-products.component.css']
})
export class SearchProductsComponent implements OnInit {

  productCode: string;
  products: Product[];

  constructor(private dataService: ProductService) { }

  ngOnInit() {
    this.productCode = "";
  }

  private searchProducts() {
    this.dataService.getProductsByProductCode(this.productCode)
      .subscribe(p => this.products = p);
  }

  onSubmit() {
    this.searchProducts();
  }

}
