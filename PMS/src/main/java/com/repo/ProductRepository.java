package com.repo;


import com.model.Product;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ProductRepository  extends CrudRepository<Product, Long> {
    List<Product> findProductsByProductCode(String code);
}
